module gitlab.com/anarcat/wallabako

go 1.15

require (
	github.com/Strubbl/wallabago v4.0.0+incompatible
	github.com/dustin/go-humanize v0.0.0-20171111073723-bb3d318650d4
	github.com/mattn/go-sqlite3 v1.6.0
	github.com/nightlyone/lockfile v0.0.0-20170804114028-6a197d5ea611
	gopkg.in/natefinch/lumberjack.v2 v2.0.0-20170531160350-a96e63847dc3
)
